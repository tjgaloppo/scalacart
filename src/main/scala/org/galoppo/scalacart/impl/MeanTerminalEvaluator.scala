package org.galoppo.scalacart.impl

import org.galoppo.scalacart.TerminalEvaluator

class MeanTerminalEvaluator extends TerminalEvaluator {
  override def evaluate(data: List[Array[Any]], valueColumn: Int): Any = {   
    data.map(_(valueColumn).asInstanceOf[Double]).sum / data.length
  }
}
