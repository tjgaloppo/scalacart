package org.galoppo.scalacart.impl

import org.galoppo.scalacart.TreeEvaluator
import org.galoppo.scalacart.tree.LeafNode
import org.galoppo.scalacart.tree.MapNode
import org.galoppo.scalacart.tree.Tree

class VotingTreeEvaluator(tree: Tree) extends TreeEvaluator(tree) {
  override def evaluate(datum: Array[Any], node: Tree): Any = {
    if (null == node || null == datum) {
      null
    } else node match {
      case leafNode: LeafNode => leafNode.value
        
      case mapNode: MapNode => {
        val columnValue = datum(mapNode.columnIndex).toString
        mapNode.valueMap.get(columnValue) match {
          case Some(subTree) => evaluate(datum, subTree)
            
          case None => {
            val map = mapNode.valueMap.values.map{
              sub => (evaluate(datum, sub), sub.weight)
            }.groupBy(_._1)
            val sums = map.map{ vote => 
              (vote._1.toString, vote._2.foldLeft(0)((sum, pair) => sum + pair._2)) 
            }
            sums.foldLeft("",-1)((best, vote) => if (vote._2 > best._2) vote else best)._1
          }
        }
      }
    }
  }
}
