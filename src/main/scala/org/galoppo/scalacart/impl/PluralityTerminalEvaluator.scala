/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.galoppo.scalacart.impl

import org.galoppo.scalacart.TerminalEvaluator

class PluralityTerminalEvaluator extends TerminalEvaluator {
  override def evaluate(data: List[Array[Any]], valueColumn: Int): Any = {
    data.groupBy(_(valueColumn).toString)
      .foldLeft(("",-1)){ (value, list) => 
          if (list._2.length > value._2) (list._1.toString, list._2.length) else value 
      }._1
  }
}
