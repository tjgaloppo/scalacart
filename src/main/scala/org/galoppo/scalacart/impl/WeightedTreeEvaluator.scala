package org.galoppo.scalacart.impl

import org.galoppo.scalacart.TreeEvaluator
import org.galoppo.scalacart.tree.{LeafNode, MapNode, Tree}


class WeightedTreeEvaluator(tree: Tree) extends TreeEvaluator(tree) {
  override protected def evaluate(datum: Array[Any], node: Tree): Any = {
    if (null == node || null == datum) {
      null
    } else node match {
      case leafNode: LeafNode => leafNode.value
        
      case mapNode: MapNode => {
        val columnValue = datum(mapNode.columnIndex).asInstanceOf[String]
        mapNode.valueMap.get(columnValue) match {
          case Some(subTree) => evaluate(datum, subTree)
            
          case None => {
              val (sum, sumWeight) = mapNode.valueMap.values.foldLeft((0.0, 0.0)){ 
                (sums, subtree) => 
                  val w = subtree.weight
                  (sums._1 + w * evaluate(datum, subtree).asInstanceOf[Double], sums._2 + w)
              }
              sum / sumWeight
          }
        } 
      }
    }
  }
}
