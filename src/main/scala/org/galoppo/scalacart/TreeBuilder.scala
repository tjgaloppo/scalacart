package org.galoppo.scalacart

import org.galoppo.scalacart.split.SplitMeasure
import org.galoppo.scalacart.tree.LeafNode
import org.galoppo.scalacart.tree.MapNode
import org.galoppo.scalacart.tree.Tree

class TreeBuilder {
  var splitMeasure: SplitMeasure = null
  var terminalEvaluator: TerminalEvaluator = null
  
  def setSplitMeasure(measure: SplitMeasure): TreeBuilder = {
    splitMeasure = measure
    this
  }
  
  def setTerminalEvaluator(evaluator: TerminalEvaluator): TreeBuilder = {
    terminalEvaluator = evaluator
    this
  }
  
  def build(data: List[Array[Any]], valueColumn: Int, maxDepth: Int): Tree = {
    val availCols = (0 until data(0).length).filter(_ != valueColumn).toList
    build(data, availCols, valueColumn, maxDepth)
  }
  
  def build(data: List[Array[Any]], availCols: List[Int], valueColumn: Int, maxDepth: Int): Tree = {
    if (data.isEmpty) {
      null
    } else if (0 == maxDepth || availCols.isEmpty || splitMeasure.evaluate(data, valueColumn) == 0.0) {
      new LeafNode(data.length, terminalEvaluator.evaluate(data, valueColumn))
    } else {
      val bestIndex = best(data, availCols, valueColumn)
      val newAvailCols = availCols.filter(_ != bestIndex)
      val split = SplitMeasure.splitByValue(data, bestIndex)
      val subTrees = split.mapValues(build(_, newAvailCols, valueColumn, maxDepth-1))
      new MapNode(data.length, bestIndex, subTrees)
    }
  }
  
  def best(data: List[Array[Any]], availCols: List[Int], valueColumn: Int): Int = {
    availCols.map(j => (j, splitMeasure.residual(data, j, valueColumn)))
      .foldLeft((-1, Double.MaxValue))((best, eval) => if (eval._2 < best._2) eval else best)._1
  }
}
