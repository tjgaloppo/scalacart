package org.galoppo.scalacart

import org.galoppo.scalacart.impl.MeanTerminalEvaluator
import org.galoppo.scalacart.impl.PluralityTerminalEvaluator
import org.galoppo.scalacart.impl.VotingTreeEvaluator
import org.galoppo.scalacart.impl.WeightedTreeEvaluator
import org.galoppo.scalacart.split.EntropySplitMeasure
import org.galoppo.scalacart.split.GiniSplitMeasure
import org.galoppo.scalacart.split.VarianceSplitMeasure

object CARTFactory {
  def learnRegressionTree(data: List[Array[Any]], valueColumn: Int, maxDepth: Int): TreeEvaluator = {
    new WeightedTreeEvaluator(new TreeBuilder()
      .setSplitMeasure(new VarianceSplitMeasure())
      .setTerminalEvaluator(new MeanTerminalEvaluator())
      .build(data, valueColumn, maxDepth))
  }
  
  def learnClassificationTreeUsingEntropy(
    data: List[Array[Any]],
    valueColumn: Int,
    maxDepth: Int): TreeEvaluator = {
    new VotingTreeEvaluator(new TreeBuilder()
      .setSplitMeasure(new EntropySplitMeasure())
      .setTerminalEvaluator(new PluralityTerminalEvaluator())
      .build(data, valueColumn, maxDepth))
  }
  
  def learnClassificationTreeUsingGini(
    data: List[Array[Any]],
    valueColumn: Int,
    maxDepth: Int): TreeEvaluator = {
    new VotingTreeEvaluator(new TreeBuilder()
      .setSplitMeasure(new GiniSplitMeasure())
      .setTerminalEvaluator(new PluralityTerminalEvaluator())
      .build(data, valueColumn, maxDepth))
  }  
}
