package org.galoppo.scalacart

import org.galoppo.scalacart.tree.Tree

abstract class TreeEvaluator(val tree: Tree) {
  def evaluate(datum: Array[Any]): Any = evaluate(datum, tree)
  
  protected def evaluate(datum: Array[Any], tree: Tree): Any
  
  override def toString: String = {
    "TreeEvaluator[%s]".format(tree)
  }
}
