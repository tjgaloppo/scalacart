package org.galoppo.scalacart.tree

abstract class Tree(val weight: Int) {
  def prettyPrint(labels: Array[String]): String = {
    prettyPrint(Some(labels), 0)
  }
  
  def prettyPrint(labels: Option[Array[String]] = None, depth: Int = 0): String
  
  protected def space(x: Int): String = {
    new String(Array.fill(x)(' '))
  }
}
