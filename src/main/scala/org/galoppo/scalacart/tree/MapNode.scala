package org.galoppo.scalacart.tree

class MapNode(weight: Int, val columnIndex: Int, val valueMap: Map[String, Tree]) 
    extends Tree(weight) {
      
  override def toString: String = {
    val b = new StringBuilder()
    "MapNode{%d}[%d]{ ".format(weight, columnIndex).addString(b)
    valueMap.foreach(pair => "%s:%s ".format(pair._1, pair._2).addString(b))
    "}".addString(b)
    b.toString
  }
  
  override def prettyPrint(labels: Option[Array[String]], depth: Int): String = {
    val b = new StringBuilder()
    (labels match {
      case Some(label) => "%s%s [%d]\n".format(space(depth), label(columnIndex), weight)
      case None => "%s%d [%d]\n".format(space(depth), columnIndex, weight)
    }).addString(b)
    valueMap.foreach{ pair => 
      "%s=%s [%d]\n%s".format(space(depth), pair._1, pair._2.weight, 
        pair._2.prettyPrint(labels, depth+3)).addString(b)
    }
    b.toString
  }
}
