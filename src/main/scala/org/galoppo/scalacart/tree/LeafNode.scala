package org.galoppo.scalacart.tree

class LeafNode(weight: Int, val value: Any) extends Tree(weight) {
  override def toString: String = {
    "LeafNode{%d}[%s]".format(weight, value)
  }
  
  override def prettyPrint(labels: Option[Array[String]], depth: Int): String = {
    "%s%s [%d]\n".format(space(depth), value, weight)
  }
}
