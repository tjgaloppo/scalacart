package org.galoppo.scalacart.split

class VarianceSplitMeasure extends SplitMeasure {
  override def evaluate(data: List[Array[Any]], valueColumn: Int): Double = {
    if (!data.isEmpty) {
      val (sx, sxx) = data.foldLeft((0.0, 0.0)){ (sums, row) => 
        val x = row(valueColumn).asInstanceOf[Double]
        (sums._1 + x, sums._2 + x * x)
      }
      val Ex = sx / data.length
      val Exx = sxx / data.length
      Exx - Ex * Ex
    } else {
      0.0
    }
  }
}
