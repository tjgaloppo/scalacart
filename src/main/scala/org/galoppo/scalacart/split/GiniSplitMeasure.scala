package org.galoppo.scalacart.split

class GiniSplitMeasure extends SplitMeasure {
  override def evaluate(data: List[Array[Any]], valueColumn: Int): Double = {
    val map = SplitMeasure.splitByValue(data, valueColumn)
    val count = map.values.map(_.length).sum.toDouble
    val sum = map.values.map{ list => {
        val p = list.length / count
        p * p
      }
    }.sum
    1.0 - sum
  }
}
