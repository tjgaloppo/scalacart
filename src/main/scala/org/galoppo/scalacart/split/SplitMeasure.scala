package org.galoppo.scalacart.split

object SplitMeasure {
  def splitByValue(data: List[Array[Any]], columnIndex: Int): Map[String, List[Array[Any]]] = {
    val map = new scala.collection.mutable.HashMap[String, List[Array[Any]]]
    var missing: List[Array[Any]] = Nil
    data.foreach{ row =>
      val columnValue = row(columnIndex).asInstanceOf[String]
      if (null != columnValue && columnValue.length > 0) {
        map.get(columnValue) match {
          case Some(list) => map.put(columnValue, row :: list)
          case None => map.put(columnValue, List(row))
        }
      } else {
        missing = row :: missing
      }
    }
  
    map.foreach(_ match { case (key, list) => map.put(key, list ::: missing) })
    
    map.toMap
  }  
}

abstract class SplitMeasure {
  def evaluate(data: List[Array[Any]], valueColumn: Int): Double
  
  def residual(data: List[Array[Any]], testColumn: Int, valueColumn: Int): Double = {
    val map = SplitMeasure.splitByValue(data, testColumn)
    val (sum, count) = map.values.foldLeft((0.0, 0)){ (sums, subList) => 
      val n = subList.length
      (sums._1 + n * evaluate(subList, valueColumn), sums._2 + n)
    }
    sum / count
  }
}
