package org.galoppo.scalacart.split

class EntropySplitMeasure extends SplitMeasure {
  override def evaluate(data: List[Array[Any]], valueColumn: Int): Double = {
    val map = SplitMeasure.splitByValue(data, valueColumn)
    val count = map.values.map(_.length).sum.toDouble
    map.values.map{ list =>
      val p = list.length / count
      -(p * math.log(p))
    }.sum
  }
}
