package org.galoppo.scalacart

abstract class TerminalEvaluator {
  def evaluate(data: List[Array[Any]], valueColumn: Int): Any
}
