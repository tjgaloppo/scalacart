package org.galoppo.scalacart

object Main {
  def main(args: Array[String]) = {   
    val data = List(
      Array(10.0, "C", "A"),
      Array(9.0, "C", "B"),
      Array(8.0, "D", "B"),
      Array(11.0, "D", "A"),
      Array(9.0, "D", "B")
    )
    // AC: 10; AD: 11; BC: 9; BD: 8.5    
    
    val tree = CARTFactory.learnRegressionTree(data, 0, -1)        
    println(tree)
    println(tree.evaluate(Array(0.0,  "D", "B")))    
    
    println(tree.tree.prettyPrint(Array("", "Col1", "Col2")))
    
    val data2: List[Array[Any]] = List(
      Array("Yes", "No", "No", "Yes", "Some", "$$$", "No", "Yes", "French", "0-10", "Yes"),
      Array("Yes", "No", "No", "Yes", "Full", "$", "No", "No", "Thai", "30-60", "No"),
      Array("No", "Yes", "No", "No", "Some", "$", "No", "No", "Burger", "0-10", "Yes"),
      Array("Yes", "No", "Yes", "Yes", "Full", "$", "Yes", "No", "Thai", "10-30", "Yes"),
				
      Array("Yes", "No", "Yes", "No", "Full", "$$$", "No", "Yes", "French", ">60", "No"),
      Array("No", "Yes", "No", "Yes", "Some", "$$", "Yes", "Yes", "Italian", "0-10", "Yes"),
      Array("No", "Yes", "No", "No", "None", "$", "Yes", "No", "Burger", "0-10", "No"),
      Array("No", "No", "No", "Yes", "Some", "$$", "Yes", "Yes", "Thai", "0-10", "Yes"),
	
      Array("No", "Yes", "Yes", "No", "Full", "$", "Yes", "No", "Burger", ">60", "No"),
      Array("Yes", "Yes", "Yes", "Yes", "Full", "$$$", "No", "Yes", "Italian", "10-30", "No"),
      Array("No", "No", "No", "No", "None", "$", "No", "No", "Thai", "0-10", "No"),
      Array("Yes", "Yes", "Yes", "Yes", "Full", "$", "No", "No", "Burger", "30-60", "Yes")           
    )

    val tree2 = CARTFactory.learnClassificationTreeUsingEntropy(data2, 10, -1)
    println(tree2)
    println(tree2.tree.prettyPrint())
    println(data2.map(row => (tree2.evaluate(row), row(10))))
  }
}
